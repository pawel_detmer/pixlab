<?php


namespace Sda\Pixlab\Config;


/**
 * Class Routing
 * @package Sda\Project\Config
 */
class Routing
{
    const MAIN_PAGE = 'mainPage';
    const PLAYERS = 'players';
    const RESULTS ='results';
}
