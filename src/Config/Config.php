<?php

namespace Sda\Pixlab\Config;

/**
 * Class Config
 * @package Sda\Pixlab\Config
 */
class Config
{
    const TEMPLATE_DIR = __DIR__ . '/../Template/templates';

    const DB_CONNECTION_DATA = [
        'dbname' => 'pixlab',
        'user' => 'PawelD',
        'password' => '',
        'host' => 'localhost',
        'driver' => 'pdo_mysql',
        'charset' => 'utf8'
    ];
}
