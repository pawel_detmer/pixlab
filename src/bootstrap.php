<?php

session_start();
require_once __DIR__ . '/../vendor/autoload.php';



use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Sda\Pixlab\Request\Request;
use Sda\Pixlab\Template\Template;
use Sda\Pixlab\Config\Config;
use Sda\Pixlab\Config\Routing;
use Sda\Pixlab\User\UserRepository;
use Sda\Pixlab\User\User;
use Sda\Pixlab\Controller\Controller;


$loader = new Twig_Loader_Filesystem(Config::TEMPLATE_DIR);
$twig = new Twig_Environment(
    $loader,
    ['cache' => false]
);

$template = new Template($twig);
$config = new Configuration();
$request = new Request();
$dbh = DriverManager::getConnection(Config::DB_CONNECTION_DATA, $config);
$repoUser = new UserRepository($dbh);

$app = new Controller($request, $repoUser, $template);

$app->run();
    
