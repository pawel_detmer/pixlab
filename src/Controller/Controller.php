<?php

namespace Sda\Pixlab\Controller;

use Sda\Pixlab\Request\Request;
use Sda\Pixlab\Template\Template;
use Sda\Pixlab\Config\Routing;
use Sda\Pixlab\User\UserRepository;
use Sda\Pixlab\User\User;

/**
 * Class Controller
 * @package Sda\Pixlab\Controller
 */
class Controller {

    /**
     * @var Request
     */
    private $request;

    /**
     * @var repoResult
     */
    private $repoUser;

    /**
     * @var Template
     */
    private $template;

    /**
     * @var array
     */
    private $params = [];

    /**
     * Controller constructor.
     * @param Request $request
     * @param UserRepository $repoUser
     * @param Template $template

     */
    public function __construct(
    Request $request, UserRepository $repoUser, Template $template
    ) {
        $this->request = $request;
        $this->repoUser = $repoUser;
        $this->template = $template;
        
    }

    public function run() {


        $action = array_key_exists('action', $_GET) ? $_GET['action'] : 'mainPage';
        $this->params['page'] = $action;


        switch ($action) {
            case Routing::MAIN_PAGE;
                $templateName = 'mainPage.html';
                $this->mainPage();
                break;
            case Routing::PLAYERS;
                $templateName = 'players.html';
                $this->players();
                break;
            case Routing::RESULTS;
                $templateName = 'results.html';
                $this->results();
                break;

            default;
                echo '404 strona nie odnaleziona';
                break;
        }

        $this->template->renderTemplate($templateName, $this->params);
    }

    private function mainPage() {
        
    }

    private function players() {
//        if (array_key_exists('players', $_POST) && $_POST['players'] == !'0') {
//
//            $playersNumber = $_POST['players'];
//
//            if (is_numeric($playersNumber) && ($playersNumber % 2 === 0) && ($playersNumber > 3)) {
//                for ($i = 1; $i <= $playersNumber; $i++) {
//
//                    $arrayPlayersNumber [] = $i;
//                    $this->params['allPlayers'] = $arrayPlayersNumber;
//                }
//
//            } else {
//                echo 'Wpisana wartość nie spełnia kryteriów: liczby całkowitej, podzielnej przez 2 i minimum 4 graczy';
//            }
//        } else {
//            echo 'Nie wpisano liczby graczy';
//        }
        var_dump($_POST);
        
        for ($numberPlayer = 1; $numberPlayer<= count($_POST); $numberPlayer++){
            if (array_key_exists('player'.$numberPlayer, $_POST)) {
                $userRegister = new User($this->request->getParamFromPost('player'.$numberPlayer));
                var_dump($userRegister);
                $this->repoUser->addUser($userRegister);
            }
        }
       
        header('Location: index.php?action=results');
        
    }

    private function results() {
        
//        for ($player = 1; $player <= count($_POST); $player++) {
//        $userRegistersArray [] = new User($_POST['player'.$player]);
//        }
//        $_SESSION['tutorial'] = $userRegistersArray;
//        
//        for ($addPlayer = 1; $addPlayer <= count($userRegistersArray); $addPlayer++){
//            $this->repoUser->addUser($userRegistersArray[$addPlayer]);
//        }

        
        
        $allPlayers = $this->repoUser->getAllplayers();
        
          for ($playerPlay = 0; $playerPlay< count($allPlayers); $playerPlay++){
              $allPlayersInTutorial = $allPlayers[$playerPlay]['nick'];
              var_dump($allPlayersInTutorial);
         
        }
    }

}
