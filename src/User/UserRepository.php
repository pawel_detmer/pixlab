<?php

namespace Sda\Pixlab\User;


use Doctrine\DBAL\Connection;
use Sda\Pixlab\User\User;
use Sda\Pixlab\Controller\Controller;


class UserRepository {
        /**
     * @var Connection
     */
    private $dbh;

    /**
     * UserRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }
//    
    public function getAllplayers () {
         $sth = $this->dbh->prepare('SELECT * FROM `players`');
         $sth->execute();
         $allUsers = $sth->fetchAll();
         
         return $allUsers;
//         if (false === $allUsers) {
//           throw new PhaseNotFoundException();
//         }
//     
//         return UserRepository::makeFromRepository($allUsers);
//         
    }
//   
//    public function getUserByEmail ($email) {
//         $sth = $this->dbh->prepare('SELECT * FROM `users` WHERE `email` =:email');
//         $sth->bindValue('email', $email, \PDO::PARAM_INT);
//         $sth->execute();
//         $emailData = $sth->fetch();
//         
//         return $emailData;
//    }
    
    public function addUser(User $user) {
     
        $sth = $this->dbh->prepare('SELECT * FROM `players` WHERE `nick` = :nick');
        $sth->bindValue('nick', $user->getNick() , \PDO::PARAM_INT);
        $sth->execute();
        $userData = $sth->fetch();
        if(false === $userData){
            return $this->dbh->insert('players', ['nick'=>$user->getNick()]);
        } else {
            echo 'uzytkownik o podaym loginie istnieje';
       }
    
    }
    
}
