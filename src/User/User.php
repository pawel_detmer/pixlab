<?php

namespace Sda\Pixlab\User;

/**
 * Description of User
 *
 * @author Paweł
 */
class User {

    private $nick;

    public function __construct($nick) {
        $this->nick = $nick;
    }

    public function getNick() {
        return $this->nick;
    }

}
