<?php

namespace Sda\Pixlab\Result;

use Doctrine\DBAL\Connection;

/**
 * Description of ResultRepository
 *
 * @author Paweł
 */
class ResultRepository {
       /**
     * @var Connection
     */
    private $dbh;
  
    /**
     * ResultRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }
}
